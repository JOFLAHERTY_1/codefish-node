
var calculator = require("../calculator");
 
describe("multiplication", function () {
  it("should multiply 2 and 3", function () {
    var product = calculator.multiply(2, 3);
    expect(product).toBe(6);
  });


it("should multiply 3 and 5", function () {
  var product = calculator.multiply(3, 5);
  expect(product).toBe(15);
});
});

describe("addition", function () {
  it("should add 2 and 3", function () {
    var sum = calculator.addition(2, 3);
    expect(sum).toBe(5);
  });

 
it("should add 10 and 12", function () {
  var sum = calculator.addition(10, 12);
  expect(sum).toBe(22);
});


});    